//
//  ViewController.swift
//  SSTextField
//
//  Created by Nicholas Lash on 12/13/17.
//  Copyright © 2017 Nicholas Lash. All rights reserved.
//

enum Question {
  case goal
  case coolness
  
  var prompt: String {
    switch self {
    case .goal:
      return "What is the meaning of life?"
    case .coolness:
      return "Why are you so cool?"
    }
  }
  
  var placeholder: String {
    switch self {
    case .goal:
      return "Goal, in three words!"
    case .coolness:
      return "Put reason here!"
    }
  }
  
  var errorMessage: String {
    switch self {
    case .goal:
      return "You are wrong."
    case .coolness:
      return "You are the most wrong!"
    }
  }
}

import UIKit



class ViewController: FormViewController {
  let cellIdentifier = "SSTextFieldTableViewCell"
  var fields: [Question] = [.goal, .coolness]
  
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
    
    
    tableView.dataSource = self
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 500
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
  }
  
}

extension ViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return fields.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? SSTextFieldTableViewCell else {
      return UITableViewCell()
    }
    cell.configure(question: fields[indexPath.row], indexPath: indexPath)
    cell.delegate = self
    return cell
  }
}

extension ViewController: SSTextFieldTableViewCellDelegate {
  func textFieldReturned(indexPath: IndexPath) {
    if indexPath.row < fields.count - 1 {
      let row = indexPath.row + 1
      let newIndexPath = IndexPath(row: row, section: 0)
      
      if newIndexPath.row == fields.count - 1 {
        guard let cell = tableView.cellForRow(at: newIndexPath) as? SSTextFieldTableViewCell else {
          return
        }
        cell.setupFinalField()
      }
      
      tableView.cellForRow(at: newIndexPath)!.becomeFirstResponder()
    }
  }
}


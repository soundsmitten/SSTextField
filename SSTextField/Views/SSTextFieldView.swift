//
//  SSTextFieldView.swift
//  SSTextField
//
//  Created by Nicholas Lash on 12/13/17.
//  Copyright © 2017 Nicholas Lash. All rights reserved.
//

import UIKit

class SSTextFieldView: NibDefinedView {

  @IBOutlet weak var promptLabel: UILabel!
  @IBOutlet weak var answerField: SSTextField!
  @IBOutlet weak var errorMessageLabel: UILabel!
  
  func configure(prompt: String, placeholder: String, errorMessage: String) {
    promptLabel.text = prompt
    answerField.placeholder = placeholder
    errorMessageLabel.text = errorMessage
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    updateConstraints()
  }
}

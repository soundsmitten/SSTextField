//
//  SSTextFieldTableViewCell.swift
//  SSTextField
//
//  Created by Nicholas Lash on 12/13/17.
//  Copyright © 2017 Nicholas Lash. All rights reserved.
//

import UIKit

protocol SSTextFieldTableViewCellDelegate {
  func textFieldReturned(indexPath: IndexPath)
}

class SSTextFieldTableViewCell: UITableViewCell {

  @IBOutlet weak var promptLabel: UILabel!
  @IBOutlet weak var textField: SSTextField!
  @IBOutlet weak var errorLabel: UILabel!
  var delegate: SSTextFieldTableViewCellDelegate?
  var indexPath: IndexPath!
  
  func configure(question: Question, indexPath: IndexPath) {
    textField.delegate = self
    self.indexPath = indexPath
    
    promptLabel.text = question.prompt
    textField.placeholder = question.placeholder
    errorLabel.text = question.errorMessage
  }
  
  override func becomeFirstResponder() -> Bool {
    textField.becomeFirstResponder()
    return true
  }
  
  
  func setupFinalField() {
    textField.returnKeyType = .done
  }
}

extension SSTextFieldTableViewCell: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    delegate?.textFieldReturned(indexPath: indexPath)
    return true
  }
}

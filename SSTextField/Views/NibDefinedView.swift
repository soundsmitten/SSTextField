//
//  NibDefinedView.swift
//  SSTextField
//
//  Created by Nicholas Lash on 12/13/17.
//  Copyright © 2017 Nicholas Lash. All rights reserved.
//

import UIKit

protocol NibDefinedBaseView: AnyObject {
  var nibName: String { get }
  func xibSetup()
  func connectXib(xibView: UIView)
}

extension NibDefinedBaseView where Self: UIView {
  func xibSetup() {
    guard let view = loadViewFromNib() else { return }
    view.frame = bounds
    view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    addSubview(view)
    connectXib(xibView: view)
  }
  
  func loadViewFromNib() -> UIView? {
    let nib = UINib(nibName: nibName, bundle: nil)
    return nib.instantiate(withOwner: self, options: nil).first as? UIView
  }
}

class NibDefinedView: UIView, NibDefinedBaseView {
  private var contentView: UIView?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    xibSetup()
  }
  
  // IF YOU SUBCLASS > 2x YOU MUST OVERRIDE THIS
  var nibName: String {
    return String(describing: type(of: self)).components(separatedBy: ".").last!
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    xibSetup()
  }
  
  func connectXib(xibView: UIView) {
    contentView = xibView
  }
}

// example of non UIView base class UI extenstion
class NibDefinedCollectionViewCell: UICollectionViewCell, NibDefinedBaseView {
  // IF YOU SUBCLASS > 2x YOU MUST OVERRIDE THIS
  var nibName: String {
    return String(describing: type(of: self)).components(separatedBy: ".").last!
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    xibSetup()
  }
  
  func connectXib(xibView: UIView) {
    contentView.addSubview(xibView)
  }
}

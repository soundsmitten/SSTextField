//
//  FormViewController.swift
//  SSTextField
//
//  Created by Nicholas Lash on 12/13/17.
//  Copyright © 2017 Nicholas Lash. All rights reserved.
//

import UIKit

class FormViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  var originalInset: UIEdgeInsets!
  
  override func viewDidLoad() {
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
    originalInset = tableView.contentInset
    tableView.separatorStyle = .none
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIContentSizeCategoryDidChange, object: nil)
    NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillShow, object: nil)
    NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillHide, object: nil)
  }
  
  @objc func keyboardWillShow(notification: Notification) {
    guard
      let keyInfo = notification.userInfo,
      let keyInfoFrame = keyInfo[UIKeyboardFrameEndUserInfoKey] as? CGRect else {
        return
    }
    
    let keyboardFrame = tableView.convert(keyInfoFrame, from: nil)
    let intersect = keyboardFrame.intersection(tableView.bounds)
    
    guard !intersect.isNull else {
      return
    }
    
    guard
      !intersect.isNull,
      let duration = keyInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double,
      let curveInfo = keyInfo[UIKeyboardAnimationCurveUserInfoKey] as? UInt else {
        return
    }
    
    let curve = UIViewAnimationOptions(rawValue: curveInfo << 16)

    UIView.animate(withDuration: duration, delay: 0.0, options: [curve], animations: { [weak self] in
      self?.tableView.contentInset = UIEdgeInsetsMake(0, 0, intersect.size.height, 0)
      self?.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, intersect.size.height, 0)
    }, completion: nil)
  }
  
  @objc func keyboardWillHide(notification: Notification) {
    guard
      let keyInfo = notification.userInfo,
      let duration = keyInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double,
      let curveInfo = keyInfo[UIKeyboardAnimationCurveUserInfoKey] as? UInt else {
        return
    }
    
    let curve = UIViewAnimationOptions(rawValue: curveInfo << 16)
    
    UIView.animate(withDuration: duration, delay: 0.0, options: [curve], animations: { [weak self] in
      guard let this = self else {
        return
      }
      this.tableView.contentInset = this.originalInset
    }, completion: nil)
  }
}

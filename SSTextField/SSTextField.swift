import UIKit

class SSTextField: UITextField {
  enum Direction {
    case forward
    case backward
  }
  
  var isFrameSet: Bool = false
  var bottomLine: UIView?
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    if isFrameSet {
      bottomLine?.removeFromSuperview()
    }
    
    setupAppearance()
    isFrameSet = true
  }
  
  func setupAppearance() {
    //font = UIFont.preferredFont(forTextStyle: .body)
    
    backgroundColor = .clear
    borderStyle = .none
    
    bottomLine = UIView()
    guard let bottomLine = bottomLine else {
      return
    }
    
    let height = 1.0
    bottomLine.backgroundColor = isFirstResponder ? .blue : .gray
    bottomLine.alpha = 0.5
    bottomLine.frame = CGRect(x: 0, y: Double(frame.height) - height, width: Double(frame.width), height: height)
    addSubview(bottomLine)
  }
  
  override func becomeFirstResponder() -> Bool {
    super.becomeFirstResponder()
    transition(direction: .forward)
    
    return true
  }
  
  override func resignFirstResponder() -> Bool {
    super.resignFirstResponder()
    transition(direction: .backward)
    
    return true
  }
  
  func transition(direction: Direction) {
    guard let bottomLine = bottomLine else {
      return
    }
    
    UIView.animate(withDuration: 0.5, animations: {
      bottomLine.alpha = direction == .forward ? 1 : 0.5
      bottomLine.backgroundColor = direction == .forward ? .blue : .gray
    })
  }
}
